#!/bin/bash

################################################################################
# Configuration for job

readonly PROJECT_ROOT="$(pwd)"

# These variables specify the input/output of the job.
readonly JOB_INPUT="${PROJECT_ROOT?}/${PARTICLE_BUILD_IN?}"
readonly JOB_OUTPUT="${PROJECT_ROOT?}/${PARTICLE_BUILD_OUT?}"

#
################################################################################

################################################################################
# Constants for buildpack-particle-firmware

# These variables define the interaction with:
# particle/buildpack-particle-firmware
#
# /input - should contain all project files
# /output - after build will contain logs and build artifacts
# /cache - temp directory to store intermediate files
# /log - directory containing run logs
# /ssh - directory containing SSH keys (will be copied to ~/.ssh)
readonly WORK_INPUT='/input/'
readonly WORK_OUTPUT='/output/'
readonly WORK_CACHE='/cache/'
readonly WORK_LOG='/log/'
readonly WORK_SSH='/ssh/'

#
################################################################################

particle_build_pre(){
  cp -T -R "${JOB_INPUT}" "${WORK_INPUT}"
}

particle_build_run(){
  /bin/run
}

particle_build_deploy(){
  cp -T -R "${WORK_OUTPUT}" "${JOB_OUTPUT}"
  cp -T -R "${WORK_LOG}" "${JOB_OUTPUT}"
}

particle_build(){
  particle_build_pre
  particle_build_run
  particle_build_deploy
}

# Bash Strict Mode
set -eu -o pipefail
IFS=$'\n\t'

particle_build
